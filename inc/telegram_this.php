<?php


function telegram_this( $id, $message, $photo=null ){
	
	$query = [ 'do'=>'send', 'id'=>$id, 'message'=>$message ];
	if( $photo )
		$query['photo'] = $photo;

	$res = file_get_contents('https://classic-labs.ezze.li/telegram/', false, stream_context_create([
		'http' => [
		    'header'  => "Content-Type: application/x-www-form-urlencoded\r\n".
						 "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36\r\n",
		    'method'  => 'POST',
		    'content' => http_build_query($query),
		],
	    'ssl' => [
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
	    ],
	]));

	return $res == 'OK';

}


