<?php

# 20 Jul 2020

function fgct( $url, $timeout=40 ){


	$contx = stream_context_create([

		'http' => [
			// 'timeout' => intval($timeout),
			'header' => "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36\r\n",
		],

	    'ssl' => [
	        'verify_peer'=>false,
	        'verify_peer_name'=>false,
	    ],

    ]);
    
	$c = file_get_contents( $url, false, $contx );
	

	if( substr($c, 0, 22) == "<br />\n<b>Warning</b>:" ){
		echo $c;
		return false;

	} else {
		return $c;
	}


}
