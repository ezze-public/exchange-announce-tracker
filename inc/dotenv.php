<?php

# dotenv
# 2022-07-13

if( file_exists('.env') ){
	
	$file = file('.env');

	if( sizeof($file) ){

		foreach( $file as $line ){
		
			if(! $line = trim($line,"\r\t\n ") )
				continue;
				
			if( substr($line, 0, 1) == '#' )
				continue;
				
			if(! strstr($line, '=') )
				continue;

			$pos = strpos($line, '=');
			
			$k = substr($line, 0, $pos);
			$v = substr($line, $pos+1 );
			
			define($k, $v);

		}

	}
	
}


