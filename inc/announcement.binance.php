<?php

include_once('simplehtmldom-master/HtmlWeb.php');
use simplehtmldom\HtmlWeb;


/**
 * 
 */
class Binance
{
	

	public static $last_md5_s = [];

	
	public static function sync(){
		
		$list = self::fetch();

		if( sizeof($list) ){

			#
			# starting
			if(! sizeof(self::$last_md5_s) ){
				echo "starting ".__CLASS__."\n";
				foreach( $list as $link => $g ){
					self::$last_md5_s[] = md5($link);
				}
			}

			#
			# main loop
			foreach( $list as $link => $item ){

				extract($item);

				if(! in_array( md5($link), self::$last_md5_s ) ){
					
					self::$last_md5_s[] = md5($link);

					if( !stristr($name, 'list') and !stristr($name, 'adds') ){
						echo "the word list/adds not used in the title\n";
					
					} else if( stristr($name, 'margin') ){
						echo "the word margin is used in the title\n";

					} else {

						$doc = new HtmlWeb();
						$html = $doc->load($link);

						foreach( $html->find('style') as $node ){
							$node->outertext = '';
						}
						$html->load($html->save()); 

						foreach($html->find('article') as $e){

							$coin = $e->outertext;
							$coin = strip_tags($coin);
							$coin = trim($coin);
							$coin = strtoupper($coin);

							if( strstr($coin, '/USDT') ){
								
								$pair_s = [];

								$the_coins = explode('/USDT', $coin);
								for( $i=0; $i<sizeof($the_coins)-1; $i++ ){
									$the_coin = $the_coins[$i];
									$the_coin = substr(strrchr($the_coin, ' '), 1);
									$the_coin.= '/USDT';
									$pair_s[] = $the_coin;
								}
								
								if( sizeof($pair_s) )
									msg_now( $by, __CLASS__, $name, $link, $pair_s );

							}

						}

					}

				}

			}

		}

	}



	public static function fetch(){
		return array_merge( self::fetch_from_news(), self::fetch_from_telegram() );
	}
	
	
	
	public static function fetch_from_news(){
		
		$list = [];
		
		$code = fgct('https://www.binance.com/en/support/announcement/c-48?nocache='.date('U').'&navId=48');

		$code = fetchInfo($code, '<script id="__APP_DATA" type="application/json">', '</script>', true);
		$code = json_decode($code, true);
		
		$code = $code['routeProps']['b723']['catalogs'];

		foreach( $code as $rw ){
			if( $rw['catalogId'] == 48 ){
				foreach( $rw['articles'] as $article ){
					$name = $article['title'];
					$link = 'https://www.binance.com/en/support/announcement/'.$article['code'];
					$list[ $link ] = [ 'name'=>$name, 'by'=>'news' ];
				}
			}
		}

 		return $list;

	}



	public static function fetch_from_telegram(){

 		$list = [];
		
		if(! $code = file_get_contents('https://t.me/s/binance_announcements') ){
			e(__CLASS__.'::'.__FUNCTION__.':'.__LINE__.', no content in telegram channel');

		} else {

			$code = text_between($code, '<section class="tgme_channel_history js-message_history">', '</section>');
			$line_s = explode('<div class="tgme_widget_message_footer compact js-message_footer">', $code);

			foreach( $line_s as $line ){
				if( !strstr($line, 'service_message') and strstr($line,'<div class="tgme_widget_message_text js-message_text" dir="auto">') ){
					
					$line = explode('<div class="tgme_widget_message_text js-message_text" dir="auto">', $line)[1];
					
					list( $name, $link ) = explode('<br/>', $line);
					$link = trim(strip_tags($link), "\r\n\t ");
					$link = trim($link, "\r\n\t ");
					
					$list[ $link ] = [ 'name'=>$name, 'by'=>'telegram' ];

				}
			}

			$list = array_reverse($list);

		}

		return $list;

	}


	public static function fetchNameFromTitle( $title ){
		return '';
	}


}




