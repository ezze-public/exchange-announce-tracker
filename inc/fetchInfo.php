<?php

function fetchInfo_s( $html, $start, $end, $skiptags=false ){

	$info_s = [];

	if( strstr($html, $start) and strstr($html, $end) ){

		$item_s = explode($start, $html);
		array_shift($item_s);

		foreach( $item_s as $item ){
			$info = explode($end, $item)[0];
			if(! $skiptags ) $info = strip_tags($info);
			$info = str_replace('&nbsp;', ' ', $info);
			$info = trim($info, "\r\n\t ");
			$info = str_replace(["'", '&#39;'], "", $info);
			$info_s[] = $info;
		}

	}

	return $info_s;

}
function fetchInfo( $html, $start, $end, $skiptags=false ) {
	return fetchInfo_s( $html, $start, $end, $skiptags )[0];
}
