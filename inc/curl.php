<?php



function curl( $url, $timeout=10, $user_agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Safari/605.1.15" ){

    $ch = curl_init( $url );
	curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

    // curl_setopt($ch, CURLOPT_HEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, $user_agent );
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
		'Accept-Language: en-US,en;q=0.9,it;q=0.8',
	]);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;

}



