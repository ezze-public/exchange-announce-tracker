<?php

# 2022-07-14


ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE);

$reserved_files = [ 'dotenv' ];


foreach( $reserved_files as $file ){
	if( file_exists('inc/'.$file.'.php') )
		include_once('inc/'.$file.'.php');
}


foreach( glob('inc/*.php') as $file ){
	if(! in_array($file, get_included_files()) ){
		include_once($file);
	}
}


