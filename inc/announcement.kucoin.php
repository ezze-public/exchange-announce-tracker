<?php

include_once('simplehtmldom-master/HtmlWeb.php');
use simplehtmldom\HtmlWeb;


/**
 * 
 */
class KuCoin
{
	

	public static $last_md5_s = [];

	
	public static function sync(){
		
		$list = self::fetch();

		if( sizeof($list) ){
		
			#
			# starting
			if(! sizeof(self::$last_md5_s) ){
				echo "starting ".__CLASS__."\n";
				foreach( $list as $link => $g ){
					self::$last_md5_s[] = md5($link);
				}
			}

			#
			# main loop
			foreach( $list as $link => $item ){

				extract($item);

				if(! in_array( md5($link), self::$last_md5_s ) ){
					self::$last_md5_s[] = md5($link);
					msg_now($by, __CLASS__, $name, $link, $pair_s);
				}

			}

		} else {
			echo "nothing.";
		}

	}



	public static function fetch(){
		return array_merge( self::fetch_from_news() , self::fetch_from_telegram() );
	}
	
	
	
	public static function fetch_from_news(){ // return [];

		$list = [];

		$doc = new HtmlWeb();
		$html = $doc->load('https://www.kucoin.com/rss/news?lang=en');
		
		if(! strstr($html, '<item>') ){
			echo "no item found in kucoin rss\n";
			
		} else foreach( $html->find('item') as $item ){

			$item = str_replace('​', '', $item);

			$date = text_between($item, '<pubdate>', '</pubdate>');
			$date = strtotime($date);

			$name = text_between($item, '<title>', '</title>');
			$name = trim( str_replace(['<![CDATA[', ']]>'], '', $name) );

			$link = text_between($item, '<link>', '</link>');

			if( $date > date('U') - 300 ){


				$description = text_between($item, '<content:encoded>', '</content:encoded>');
				$description = strip_tags($description);
				$description = trim( str_replace(['<![CDATA[', ']]>'], '', $description) );

				
				if( 
					( stristr($name, 'gets listed') or ( stristr($name, 'adds') ) ) and
					// strstr($name, '/USDT') and
					!stristr($name, 'margin')
				){

					if( strstr($description, '/USDT') ){
						
						$pair_arr = explode('/USDT', $description);
						$pair_s = [];
						for( $i=0; $i<sizeof($pair_arr)-1; $i++ ){
							$pair_s[] = substr(strrchr($pair_arr[$i], ' '), 1).'/USDT';
						}

						$list[ $link ] = [ 'name'=>$name, 'pair_s'=>$pair_s, 'by'=>'news' ];

					}
				}

			}
			
		}
		
 		return $list;

	}



	public static function fetch_from_telegram(){ // return [];
		
		$list = [];

		if(! $code = fgct('https://classic-labs.ezze.li/telegram-channel/?channel_name=Kucoin_news&count=1&only_message=1') ){
			e(__CLASS__.'::'.__FUNCTION__.':'.__LINE__.', no content in telegram channel');
		}

		$line_s = json_decode($code, true);


		foreach( $line_s as $line ){

			$line = preg_replace("/[^\r\nA-Za-z0-9 \/\.\-\:\,\(\)!?_=%#$&*~\[\]\{\}|]/", " ", $line);
			$line = trim($line);
			while( strstr($line, '  ') )
				$line = str_replace('  ', ' ', $line);

			if( ( stristr($line, 'gets listed') or stristr($line, ' adds ') ) and stristr($line, '/USDT') and !stristr($line, 'margin') and !stristr($line, 'Giveaway') and stristr($line, 'https://www.kucoin.com') ){

				$pair_arr = strip_tags($line);
				$pair_arr = explode('/USDT', $pair_arr);
				$pair_s = [];
				for( $i=0; $i<sizeof($pair_arr)-1; $i++ ){

					$t0 = strrev($pair_arr[$i]);
					$t1 = '';
					for( $i=0; $i<strlen($t0); $i++ ){
						if(! ctype_alpha($t0[$i]) ) break;
						$t1.= $t0[$i];
					}
					$pair_s[] = strrev($t1) .'/USDT';

				}

				$link = explode('https://www.kucoin.com', $line)[1];
				$link = explode(' ', $link)[0];
				$link = 'https://www.kucoin.com'.$link;

				$name = explode('https://www.kucoin.com', $line)[0];
				$name = str_replace(["\r\n", "\r", "\n"], ' ', $name);
				while( strstr($name, '  ') )
					$name = str_replace('  ', ' ', $name);
				$name = trim($name, '\r\n\t ');
				
				$list[ $link ] = [ 'name'=>$name, 'pair_s'=>$pair_s, 'by'=>'telegram' ];

			}

		}

		return $list;

	}



	public static function fetchNameFromTitle( $title ){
		
		// Cult DAO (CULT) Gets Listed on KuCoin!
		$name = explode(' (', $title)[0];
		$name = trim($name);

		return $name;

	}


}




