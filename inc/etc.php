<?php


function e( $text='' ){

	$bt0 = debug_backtrace()[0];
	$bt1 = debug_backtrace()[1];
	$text = date('Y-m-d H:i:s').', '.$bt1['function'].':'.$bt0['line'].( $text ? '; '.$text : '' )."\n";

	echo $text;

}


function text_between( $text, $start, $end ){

	if( strstr($text, $start) and strstr($text, $end) ){

	    $text = explode($start, $text)[1];
    	$text = explode($end, $text)[0];

	    return $text;

    } else {
    	return '';
    }

}


function sleepdot( $sec, $dot=".", $end="\n" ){
    for( $i=0; $i<= $sec; $i++ ){
        echo $dot;
        sleep(1);
    }
	echo $end;
}


function semaphore( $proc ){
	
	if(! is_array($proc) ){
		$proc = [$proc];
	}
	
	if( sizeof( $curr = curr_proc($proc) ) >= 2 ){
		// echo "semaphore lock\n";
		// var_dump($curr);
		die;
	}
	
}


function strip_doublespaces( $text ){
	
	while( stristr($text, '  ') ){
		$text = str_replace('  ', ' ', $text);
	}
	
	return $text;
	
}










