<?php

include_once('simplehtmldom-master/HtmlWeb.php');
use simplehtmldom\HtmlWeb;


/**
 * 
 */
class Huobi
{
	

	public static $last_md5_s = [];

	
	public static function sync(){
		
		$list = self::fetch();

		if( sizeof($list) ){

			#
			# starting
			if(! sizeof(self::$last_md5_s) ){
				echo "starting ".__CLASS__."\n";
				foreach( $list as $link => $g ){
					self::$last_md5_s[] = md5($link);
				}
			}

			#
			# main loop
			foreach( $list as $link => $item ){

				extract($item);
				
				if(! in_array( md5($link), self::$last_md5_s ) ){

					self::$last_md5_s[] = md5($link);

					if( !stristr($name, 'open trading') and !stristr($name, 'will list') ){
						echo "the word List not used in title\n";
					
					} else {

						sleep(1);
						$html = curl($link);
						
						if( stristr($html, 'server error') ){
							echo "ERROR !\n\n";
							continue;
						
						} else if(! stristr($html, '</head>') ){
							echo "<pre>wrong content in html: ($html)\nlink: ($link)</pre>";
							continue;

						} else {
							
							$coin = explode('</head>', $html)[1];
							// echo $coin;

							$coin = trim($coin);
							$coin = strip_tags($coin);
							$coin = trim($coin);
							$coin = strtoupper($coin);
							$coin = str_replace(['(',')'], ' ', $coin);
							$coin = strip_doublespaces($coin);

							// echo "\n\n- - - - -\n\n".$coin;
							// continue;

							if( strstr($coin, '/USDT') ){
								
								$pair_s = [];

								$the_coins = explode('/USDT', $coin);
								for( $i=0; $i<sizeof($the_coins)-1; $i++ ){
									$the_coin = $the_coins[$i];
									$the_coin = substr(strrchr($the_coin, ' '), 1);
									$the_coin = trim($the_coin, "\r\n\t ");
									$the_coin.= '/USDT';
									if(! in_array($the_coin, $pair_s) )
										$pair_s[] = $the_coin;
								}
								
								if( sizeof($pair_s) )
									msg_now( $by, __CLASS__, $name, $link, $pair_s );

							}


						}

					}

				}

			}

		}

	}



	public static function fetch(){
		return array_merge( self::fetch_from_news() , self::fetch_from_telegram() );
	}
	
	
	
	public static function fetch_from_news(){ // return [];

		$list = [];

		$url = 'https://www.huobi.com/support/en-us/search?keyword=Will%20List';
		$html = curl($url);

		if( stristr($html, 'server error') ){
			echo "ERROR !\n\n";
		
		} else {

			$html = fetchInfo($html, '<dl class="list"', '</dl>', true);
			$item_s = fetchInfo_s($html, '<dd class="list-item"', '</dd>', true);
			
			$j = 0;
			foreach( $item_s as $item ){
				
				$link = fetchInfo($item, '<a href="', '"');
				$link = trim($link, "\r\n\t ");
				$name = trim(strip_tags('<div '.fetchInfo($item, '<div class="link-dealpair"', '</a>', true)));

				$list[ 'https://www.huobi.com'.$link ] = [ 'name'=>$name, 'by'=>'news' ];

				if( ++$j >= 4 )
					break;

			}

		}

 		return $list;

	}



	public static function fetch_from_telegram(){ // return [];
		
		$list = [];

		if(! $code = fgct('https://classic-labs.ezze.li/telegram-channel/?channel_name=HuobiGlobalAnnouncementChannel&count=20&only_message=1') ){
			e(__CLASS__.'::'.__FUNCTION__.':'.__LINE__.', no content in telegram channel');
		}

		$line_s = json_decode($code, true);


		foreach( $line_s as $line ){

			$line = preg_replace("/[^\r\nA-Za-z0-9 \/\.\-\:\,\(\)!?_=%#$&*~\[\]\{\}|]/", " ", $line);
			$line = trim($line);
			while( strstr($line, '  ') )
				$line = str_replace('  ', ' ', $line);

			if( 
				( stristr($line, 'gets listed') or stristr($line, ' adds ') or stristr($line, ' list ') )
				and ( stristr($line, '/USDT') or stristr($line, 'will list ') )
				and !stristr($line, 'margin')
				and !stristr($line, 'Giveaway')
				and !stristr($line, ' bot ')
				and stristr($line, 'https://')
			){

				$link = explode('https://', $line)[1];
				$link = explode(' ', $link)[0];
				$link = 'https://'.$link;
				$link = str_replace('https://www.huobi.co.no', 'https://www.huobi.com', $link);
				$link = trim($link, "\r\n\t ");

				$name = explode('https://', $line)[0];
				$name = str_replace(["\r\n", "\r", "\n"], ' ', $name);
				while( strstr($name, '  ') )
					$name = str_replace('  ', ' ', $name);
				$name = trim($name, '\r\n\t ');
				
				$list[ $link ] = [ 'name'=>$name, 'by'=>'telegram' ];

			}

		}

		return $list;

	}



	public static function fetchNameFromTitle( $title ){
			
		// Huobi Global Will List OGV (Origin Protocol) on July 12, 2022
		$name = fetchInfo($title, ' (', ') on ');
		$name = trim($name);
		
		return $name;

	}



}




