<?php

# 2020-11-25
# 1.2


function curl_post( $url , $params=null, $method="POST", $timeout=null ){

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	curl_setopt($ch, CURLOPT_URL, $url );
	
	if( sizeof($params) ){
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params) );
	}

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	if( $timeout ) curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

	$server_output = curl_exec ($ch);
	curl_close ($ch);

	return $server_output;

}


function curl( $url ){


	$options = [
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_FOLLOWLOCATION => true,
	    CURLOPT_SSL_VERIFYHOST => false,
	    CURLOPT_SSL_VERIFYPEER => false,

	    CURLOPT_COOKIEJAR => '/tmp/cookie/'.session_id(),
		CURLOPT_COOKIEFILE => '/tmp/cookie/'.session_id(),
		
		CURLOPT_RESOLVE => [ "support.lbank.site:443:104.18.249.37" ],
		CURLOPT_VERBOSE => true,

	    CURLOPT_HTTPHEADER => [
	        'accept: application/json, text/plain, */*',
	        'Accept-Language: en-US,en;q=0.5',
	        'x-application-type: WebClient',
	        'x-client-version: 2.10.4',
	        // 'Origin: https://www.googe.com',
	        'user-agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0',
	    ]
	];

	$ch = curl_init();
	curl_setopt_array($ch, $options);
	$result = curl_exec($ch);
	curl_close($ch);
	print_r($result);


}

function curl_kk( $url, $timeout=10 ){

    $ch = curl_init();


	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTP_VERSION, 1.1);


	curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);


	$headers = [
	    // 'Host: support.lbank.info',
	    'User-Agent: '.$_SERVER['HTTP_USER_AGENT'],	    
	];

	if( isset($_SERVER['HTTP_ACCEPT_ENCODING']) )
		$headers[] = 'Accept-Encoding: '.$_SERVER['HTTP_ACCEPT_ENCODING'];
	
	if( isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) )
		$headers[] = 'Accept-Language: '.$_SERVER['HTTP_ACCEPT_LANGUAGE'];
	
	if( isset($_SERVER['HTTP_COOKIE']) )
		$headers[] = 'Cookie: '.$_SERVER['HTTP_COOKIE'];
	
	if( isset($_SERVER['HTTP_ACCEPT']) )
		$headers[] = 'Accept: '.$_SERVER['HTTP_ACCEPT'];

	if( isset($_SERVER['HTTP_CONNECTION']) )
		$headers[] = 'Connection: '.$_SERVER['HTTP_CONNECTION'];

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/cookie/'.session_id() ); 
	curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookie/'.session_id() ); 
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADERFUNCTION, "HandleHeaderLine");



    $output = curl_exec($ch);
    curl_close($ch);

    return $output;

}




function HandleHeaderLine( $curl, $header_line ) {
	echo $header_line; // or do whatever
	return strlen($header_line);
}

















$url = 'https://support.lbank.site/hc/en-gb/sections/360000352374-Listing-Announcement';
$data = OpenURLcloudflare($url);
print $data;



function OpenURLcloudflare($url) {
    //get cloudflare ChallengeForm
    $data = OpenURL($url);
    preg_match('/<form id="ChallengeForm" .+ name="act" value="(.+)".+name="jschl_vc" value="(.+)".+<\/form>.+jschl_answer.+\(([0-9\+\-\*]+)\);/Uis',$data,$out);
    if(count($out)>0) {
        eval("\$jschl_answer=$out[3];");
        $post['act']            = $out[1];
        $post['jschl_vc']        = $out[2];
        $post['jschl_answer']    = $jschl_answer;
        //send jschl_answer to the website
        $data = OpenURL($url, $post);
    }
    return($data);
}

function OpenURL($url, $post=array()) {
    $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:13.0) Gecko/20100101 Firefox/13.0.1';
    $headers[] = 'Accept: application/json, text/javascript, */*; q=0.01';
    $headers[] = 'Accept-Language: ar,en;q=0.5';
    $headers[] = 'Connection: keep-alive';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    if(count($post)>0) {
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/curl.cookie');
    curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/curl.cookie');
    $data = curl_exec($ch);
    return($data);
}



