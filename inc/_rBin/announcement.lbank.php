<?php

include_once('simplehtmldom-master/HtmlWeb.php');
use simplehtmldom\HtmlWeb;


/**
 * 
 */
class Lbank
{
	

	public static $last_md5 = null;

	
	public static function sync(){
		
		$coin_s = [];
		$list = self::fetch();

		if( sizeof($list) ){

			$curr_md5 = md5(array_keys($list)[0]);

			if( $curr_md5 == self::$last_md5 ){
				echo "same: ".$curr_md5." == ".self::$last_md5."\n";

			} else {

				foreach( $list as $link => $item ){

					extract($item);

					// if( !stristr($name, 'list') and !stristr($name, 'adds') ){
					// 	// echo "the word List not used in title\n";
					// 	continue;
					// }
					
					if(! self::$last_md5 ){
					// if( 0 ){
						echo "starting\n";
						break;

					} else if( md5($link) == self::$last_md5 ){
						echo "semi: ".md5($link)." == ".self::$last_md5."\n";
						break;

					} else if( !stristr($name, 'list') and !stristr($name, 'adds') ){
						echo "the word List not used in title\n";
						continue;
					
					} else {

						// echo __CLASS__.': '.$name."\n";
						// telegram_this($by.': announcement on '.__CLASS__."\n".$name."\n\n".$link);
						
						$doc = new HtmlWeb();
						$html = $doc->load($link);


						foreach( $html->find('style') as $node ){
							$node->outertext = '';
						}
						$html->load($html->save()); 



						foreach($html->find('article') as $e){

							$coin = $e->outertext;
							$coin = strip_tags($coin);
							$coin = trim($coin);
							$coin = strtoupper($coin);

							if( strstr($coin, '/USDT') or strstr($coin, '/BUSD') ){
								
								$pair_s = [];

								foreach( ['USDT', 'BUSD'] as $pair ){
									$the_coins = explode('/'.$pair, $coin);
									for( $i=0; $i<sizeof($the_coins)-1; $i++ ){
										$the_coin = $the_coins[$i];
										$the_coin = substr(strrchr($the_coin, ' '), 1);
										$the_coin.= "/".$pair;
										$pair_s[] = $the_coin;
										$coin_s[] = $the_coin;
									}
								}
								
								telegram_this($by.': announcement on '.__CLASS__."\n".$name."\n\n".$link."\n** ".implode(', ', $pair_s)." **" );
								// die();

							}

						}

						// break;

					}

				}

				self::$last_md5 = $curr_md5;

			}

		}

		return $coin_s;

	}



	public static function fetch(){
		return array_merge( self::fetch_from_news(), self::fetch_from_telegram() );
	}
	
	
	
	public static function fetch_from_news(){
		
		$list = [];
		
		// $code = curl('https://support.lbank.site/hc/en-gb/sections/360000352374-Listing-Announcement');
		// echo $code;

		// echo cloudFlareBypass("https://support.lbank.site/hc/en-gb/sections/360000352374-Listing-Announcement");

		die();
		$code = text_between($code, '<ul class="article-list">', '</ul>');

		$list_of_announcements = explode('</a>', $code);

		if( sizeof($list_of_announcements) != 30 ){
			echo "something wrong with ".__FUNCTION__."\n";

		} else foreach( $list_of_announcements as $line ){
			$name = trim( strip_tags($line) );
			$link = text_between($line, 'href="', '"');
			$link = 'https://support.lbank.site'.$link;
			$list[ $link ] = [ 'name'=>$name, 'by'=>'news' ];
		}

 		return $list;

	}



	public static function fetch_from_telegram(){
 		return [];
	}



}




