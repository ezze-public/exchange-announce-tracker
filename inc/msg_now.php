<?php



function msg_now( $by, $ex, $news_title, $news_link, $pair_s ){
	
	$photo_s = [];

	$msg = $by." announcement on $ex\n\n";
	$msg.= "<b>$news_title</b>\n\n$news_link";
	
	foreach( $pair_s as $pair ){
		
		list( $coin, $base ) = explode('/', $pair);

		$coin_name = $ex::fetchNameFromTitle($news_title);
		$coin_detail = coingecko_req("coin-detail.php?coin={$coin}".( $coin_name ? "&name={$coin_name}" : '' ));
		$rank = $coin_detail['rank'] ? : ( $coin_detail['market_cap'] ? " #" : '' );
		if( $rank )
			$rank = " #{$rank}";
		
		$pair_ex = coingecko_req("exchange-by-coin.php?coin={$coin}");
		$pair_ex_st = sizeof($pair_ex) ? implode(', ', array_values($pair_ex)) : 'no exchange';

		$msg.= "\n\n---------\n\n<b>{$coin}</b>{$rank}\n{$pair_ex_st}";

		foreach( $pair_ex as $ex => $ex_name ){
			$ex = strtolower($ex);
			if( in_array($ex, ['bkex', 'mexc']) ){
				$photo = "https://classic-labs.ezze.li/order-console/?do=kline&ex={$ex}&base={$base}&coin={$coin}&timeframe=1h&limit=168&barchart";
				$comment = strtoupper($coin.$base)." on ".strtoupper($ex)."\n\n{$photo}";
				$photo_s[ $photo ] = $comment;
			}
		}
		
	}


	# send the main message
	telegram_this(TELEGRAM_ID, $msg);

	# send the charts
	if( sizeof($photo_s) )
		foreach( $photo_s as $photo => $msg )
			telegram_this(TELEGRAM_ID, $msg, $photo);

	# done
	return true;

}



